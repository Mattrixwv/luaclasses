--lua/luaClasses/testAlgorithms.lua
--Mathew Ellison
-- Created: 03-28-19
--Modified: 06-30-21
--This script is used to test the Algorithms script
--[[
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Algorithms";
require "Stopwatch";
local bigint = require "bigint";


local function testGetPrimes()
	local failed = false;	--Holds whether a test was failed

	--Test 1
	local correctAnswer = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
	local topNum = 100;
	local answer = getPrimes(topNum);	--Get the answer from the function
	--Print an error message if the function returned the wrong answer
	if(table.unpack(correctAnswer) ~= table.unpack(answer)) then
		io.write("getPrimes failed the first test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("getPrimes passed all tests\n");
	end
end

local function testGetNumPrimes()
	local failed = false;	--Holds whether a test was failed

	--Test 1
	local correctAnswer = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
	local numPrimes = 100;
	local answer = getNumPrimes(numPrimes);	--Get the answer from the function
	--Print an error message if the function returned the wrong answer
	if(table.unpack(correctAnswer) ~= table.unpack(answer)) then
		io.write("getNumPrimes failed the first test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("getNumPrimes passed all tests\n");
	end
end

local function testIsPrime()
	local failed = false;	--Holds whether a test was failed

	--Test 1
	local num = 2;
	local correctAnswer = true;
	local answer = isPrime(num);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("isPrime failed the first test\n");
		failed = true;
	end
	--Test 2
	num = 97;
	correctAnswer = true;
	answer = isPrime(num);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("isPrime failed the second test\n");
		failed = true;
	end
	--Test 3
	num = 1000;
	correctAnswer = false;
	answer = isPrime(num);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("isPrime failed the third test\n");
		failed = true;
	end
	--Test 4
	num = 1;
	correctAnswer = false;
	answer = isPrime(num);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("isPrime failed the fourth test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("isPrime passed all tests\n");
	end
end

local function testGetFactors()
	local failed = false;	--Holds whether a test was failed

	--Test 1
	local correctAnswer = {2, 2, 5, 5};
	local number = 100;
	local answer = getFactors(number);
	--Print an error message if the function returned the wrong answer
	if(table.unpack(correctAnswer) ~= table.unpack(answer)) then
		io.write("getFactors failed the first test\n");
		failed = true;
	end

	--Test 2
	correctAnswer = {2, 7, 7};
	number = 98;
	answer = getFactors(number);
	--Print an error message if the function returned the wrong answer
	if(table.unpack(correctAnswer) ~= table.unpack(answer)) then
		io.write("getFactors failed the second test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("getFactors passed all tests\n");
	end
end

local function testGetDivisors()
	local failed = false;	--Holds whether a test was failed

	--Test 1
	local correctAnswer = {1, 2, 4, 5, 10, 20, 25, 50, 100};
	local topNum = 100;
	local answer = getDivisors(topNum);
	--Print an error message if the function returned the wrong answer
	if(table.unpack(correctAnswer) ~= table.unpack(answer)) then
		io.write("getDivisors failed the first test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("getDivisors passed all tests\n");
	end
end

local function testGetFib()
	local failed = false;	--Holds whether a test was failed

	--Test 1
	local correctAnswer = 144;
	local number = 12;
	local answer = getFib(number);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("getFib failed the first test\n");
		failed = true;
	end

	--Test 2
	correctAnswer = 6765
	number = 20
	answer = getFib(number);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("getFib failed the second test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("getFib passed all tests\n");
	end
end

local function testGetLargeFib()
	local failed = false;	--Holds whther a test was failed

	--Test 1
	local correctAnswer = bigint.new(144);
	local number = 12;
	local answer = getLargeFib(number);
	--Print an error message if the function returned the wrong answer
	if(bigint.compare(correctAnswer, answer, "!=")) then
		io.write("getLargeFib failed the first test\n");
		failed = true;
	end

	--Test 2
	correctAnswer = bigint.new(6765);
	number = 20;
	answer = getLargeFib(number);
	--Print an error message if the function returned the wrong answer
	if(bigint.compare(correctAnswer, answer, "!=")) then
		io.write("getLargeFib failed the second test\n");
		failed = true;
	end

	--Test 3
	correctAnswer = bigint.new("1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816");
	number = 4782;
	answer = getLargeFib(number);
	--Print an error message if the function returned the wrong answer
	if(bigint.compare(correctAnswer, answer, "!=")) then
		io.write("getLargeFib failed the third test\n");
		failed = true;
	end

	--Print a message if all of the test passed
	if(not failed) then
		io.write("getLargeFib passed all tests\n");
	end
end

local function testGetSum()
	local failed = false;	--Holds whether a test was failed

	--Test 1
	local correctAnswer = 0;
	local numbers = {};
	local answer = getSum(numbers);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("getSum failed the first test\n");
		failed = true;
	end

	--Test 2
	correctAnswer = 118;
	numbers = {2, 2, 3, 3, 4, 4, 100};
	answer = getSum(numbers);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("getSum failed the second test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("getSum passed all tests\n");
	end
end

local function testGetProd()
	local failed = false;	--Holds whether a test was failed

	--Test 1
	local correctAnswer = 0;
	local numbers = {};
	local answer = getProd(numbers);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("getProd failed the first test\n");
		failed = true;
	end

	--Test 2
	correctAnswer = 57600;
	numbers = {2, 2, 3, 3, 4, 4, 100};
	answer = getProd(numbers);
	--Print an error message if the function returned the wrong answer
	if(correctAnswer ~= answer) then
		io.write("getProd failed the second test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("getProd passed all tests\n");
	end
end

local function testGetPermutations()
	local failed = false;	--Holds whether a test was failed

	--Test 1
	local permString = "012";
	local correctAnswer = {"012", "021", "102", "120", "201", "210"};
	local answer = getPermutations(permString);
	if(table.unpack(correctAnswer) ~= table.unpack(answer)) then
		io.write("getPermutatiosn failed the first test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("getPermutations passed all tests\n");
	end
end

local function testIsFound()
	local failed = false;	--Signals whether a test was failed

	--Test 1
	local numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	local correctAnswer = true;
	local answer = isFound(numbers, 0);
	if(answer ~= correctAnswer) then
		io.write("isFound failed the first test\n");
		failed = true;
	end

	--Test 2
	numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	correctAnswer = true;
	answer = isFound(numbers, 9);
	if(answer ~= correctAnswer) then
		io.write("isFound failed the second test\n");
		failed = true;
	end

	--Test 3
	numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	correctAnswer = true;
	answer = isFound(numbers, 5);
	if(answer ~= correctAnswer) then
		io.write("isFound failed the third test\n");
		failed = true;
	end

	--Test 4
	numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	correctAnswer = false;
	answer = isFound(numbers, 10);
	if(answer ~= correctAnswer) then
		io.write("isFound failed the fourth test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("isFound passed all tests\n");
	end
end

local function testGCD()
	local failed = false;	--Signals whether a test was failed

	--Test 1
	local num1 = 2;
	local num2 = 3;
	local correctAnswer = 1;
	local answer = gcd(num1, num2);
	if(answer ~= correctAnswer) then
		io.write("gcd failed the first test\n");
		failed = true;
	end

	--Test 2
	num1 = 1000;
	num2 = 575;
	correctAnswer = 25;
	answer = gcd(num1, num2);
	if(answer ~= correctAnswer) then
		io.write("gcd failed the second test\n");
		failed = true;
	end

	--Test 3
	num1 = 1000;
	num2 = 1000;
	correctAnswer = 1000;
	answer = gcd(num1, num2);
	if(answer ~= correctAnswer) then
		io.write("gcd failed the third test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("gcd passed all tests\n");
	end
end

local function testFactorial()
	local failed = false;	--Signals whether a test was failed

	--Test 1
	local num = 1;
	local correctAnswer = 1;
	local answer = factorial(num);
	if(answer ~= correctAnswer) then
		io.write("factorial failed the first test\n");
		failed = true;
	end
	--Test 2
	local num = 10;
	local correctAnswer = 3628800;
	local answer = factorial(num);
	if(answer ~= correctAnswer) then
		io.write("factorial failed the second test\n");
		failed = true;
	end
	--Test 3
	local num = -5;
	local correctAnswer = 1;
	local answer = factorial(num);
	if(answer ~= correctAnswer) then
		io.write("factorial failed the third test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("factorial passed all tests\n");
	end
end

local function testIsPalindrome()
	local failed = false;	--Signals whether a test was failed

	--Test 1
	local str = "101";
	local correctAnswer = true;
	local answer = isPalindrome(str);
	if(answer ~= correctAnswer) then
		io.write("isPalindrome failed the first test\n");
		failed = true;
	end
	--Test 2
	str = "100";
	correctAnswer = false;
	answer = isPalindrome(str);
	if(answer ~= correctAnswer) then
		io.write("isPalindrome failed the second test\n");
		failed = true;
	end
	--Test 3
	str = "";
	correctAnswer = true;
	answer = isPalindrome(str);
	if(answer ~= correctAnswer) then
		io.write("isPalindrome failed the third test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("isPalindrome passed all tests\n");
	end
end

local function testToBin()
	local failed = false;	--Signals whether a test was failed

	--Test 1
	local num = 7;
	local correctAnswer = "111";
	local answer = toBin(num);
	if(answer ~= correctAnswer) then
		io.write("toBin failed the first test\n");
		failed = true;
	end
	--Test 2
	num = 0;
	correctAnswer = "0";
	answer = toBin(num);
	if(answer ~= correctAnswer) then
		io.write("toBin failed the second test\n");
		failed = true;
	end
	--Test 3
	num = 1000000;
	correctAnswer = "11110100001001000000";
	answer = toBin(num);
	if(answer ~= correctAnswer) then
		io.write("toBin failed the third test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("toBin passed all tests\n");
	end
end

local function testPrintTable()
	local failed = false;	--Signals whether a test was failed

	--Test 1
	local nums = {};
	local correctAnswer = "[]";
	local answer = printTable(nums);
	if(answer ~= correctAnswer) then
		io.write("printTable int failed the first test\n");
	end
	--Test 2
	nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	correctAnswer = "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]";
	answer = printTable(nums);
	if(answer ~= correctAnswer) then
		io.write("printTable int failed the second test\n");
	end
	--Test 3
	nums = {-3, -2, -1, 0, 1, 2, 3};
	correctAnswer = "[-3, -2, -1, 0, 1, 2, 3]";
	answer = printTable(nums);
	if(answer ~= correctAnswer) then
		io.write("printTable int failed the third test\n");
	end

	--Test 4
	local strings = {"A", "B", "C"};
	correctAnswer = "[A, B, C]";
	answer = printTable(strings);
	if(answer ~= correctAnswer) then
		io.write("printTable string failed the first test\n");
	end
	--Test 5
	strings = {"abc", "def", "ghi"};
	correctAnswer = "[abc, def, ghi]";
	answer = printTable(strings);
	if(answer ~= correctAnswer) then
		io.write("printTable string failed the second test\n");
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("printTable passed all tests\n");
	end
end


--Create the timer to time each test
local timer = Stopwatch:create();

--Test getPrimes
timer:start();
testGetPrimes();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test getNumPrimes
timer:start();
testGetNumPrimes();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test isPrime
timer:start();
testIsPrime();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test getFactors
timer:start();
testGetFactors();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test getDivisors
timer:start();
testGetDivisors();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test getFib
timer:start();
testGetFib();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test getLargeFib
timer:start();
testGetLargeFib();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test getSum
timer:start();
testGetSum();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test getProd
timer:start();
testGetProd();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test getPermutations
timer:start();
testGetPermutations();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test isFound
timer:start();
testIsFound();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test gcd
timer:start();
testGCD();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test factorial
timer:start();
testFactorial();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test isPalindrome
timer:start();
testIsPalindrome();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test toBin
timer:start();
testToBin();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");

--Test printTable
timer:start();
testPrintTable();
timer:stop();
io.write("It took " .. timer:getString() .. " to run this test\n");
