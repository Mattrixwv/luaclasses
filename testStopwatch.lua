--luaClasses/testStopwatch.lua
--Matthew Ellison
-- Created: 03-29-19
--Modified: 03-29-19
--This script is used to test the stopwatch class
--[[
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


require "Stopwatch"


--Setup a stopwatch
timer = Stopwatch:create();

--Try to get the time before starting the timer
test = timer:getSeconds();
if(test ~= -1) then
	io.write("Failed test of getting time before starting the timer\n");
	io.write("Value should be -1 but is " .. test .. '\n');
end

--Start the timer
timer:start();

--Do a loop to run up some time
for cnt = 1, 100000 do
	cnt = cnt;
end

--Stop the timer
timer:stop();

--Test the different resolutions
baseTime = timer:getSeconds();	--Start with seconds because that is lua's native time resolution
--Test microseconds
if(timer:getMicroseconds() ~= (baseTime * 1000000)) then
	io.write("Failed microsecond test\n");
else
	io.write("Passed the microsecond test\n");
end

--Test milliseconds
if(timer:getMilliseconds() ~= (baseTime * 1000)) then
	io.write("Failed millisecond test\n");
else
	io.write("Passed the millisecond test\n");
end

--Test Minutes
if(timer:getMinutes() ~= (baseTime / 60)) then
	io.write("Failed minute test\n");
else
	io.write("Passed the minute test\n");
end

--Test Hours
if(timer:getHours() ~= (baseTime / 3600)) then
	io.write("Failed hour test\n");
else
	io.write("Passed the hour test\n");
end

--Print the results
io.write("\nHere is a printing of the different times, starting with the string:\n");
io.write("It took " .. timer:getString() .. " to run this algorithm\n");
io.write("It took " .. timer:getMicroseconds() .. " microseconds to run this algorithm\n");
io.write("It took " .. timer:getMilliseconds() .. " milliseconds to run this algorithm\n");
io.write("It took " .. timer:getSeconds() .. " seconds to run this algorithm\n");
io.write("It took " .. timer:getMinutes() .. " minutes to run this algorithm\n");
io.write("It took " .. timer:getHours() .. " hours to run this algorithm\n");

--[[ Results:
Passed the microsecond test
Passed the millisecond test
Passed the minute test
Passed the hour test

Here is a printing of the different times, starting with the string:
It took 383 microseconds to run this algorithm
It took 383.0 microseconds to run this algorithm
It took 0.383 milliseconds to run this algorithm
It took 0.000383 seconds to run this algorithm
It took 6.3833333333333e-06 minutes to run this algorithm
It took 1.0638888888889e-07 hours to run this algorithm
]]
