--luaClasses/Stopwatch.lua
--Matthew Ellison
-- Created: 02-01-19
--Modified: 06-30-21
--This is a simple class to be used to time runtimes of various things within programs
--[[
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


--You need some kind of table to hold the times
Stopwatch = {
	timeResolution = {
		microSeconds = 1;
		milliSeconds = 2;
		seconds = 3;
		minutes = 4;
		hours = 5;
	};
};
Stopwatch.__index = Stopwatch;

--This is needed to create a Stopwatch and link the variables and functions
--It should be called before anything else when creating a Stopwatch
function Stopwatch:create()
	local timer = {
		startTime = nil,
		stopTime = nil
	};
	setmetatable(timer, Stopwatch);	--This links the new variable with the functions from the Stopwatch
	return timer;
end

--This function gets the start time to simulate actually hitting the button on a stopwatch
function Stopwatch:start()
	self.stopTime = nil;	--Make sure it knows that the clock is still running
	--Setting the clock last puts the time closer to the actual start of the thing you are trying to time
	self.startTime = os.clock();	--Get the current time
end

--This function gets the stop time to simulate actually hitting the stop button on a stopwatch
function Stopwatch:stop()
	--Set the time first, for efficiency sake
	self.stopTime = os.clock();
	--Make sure the clock had been started
	if(self.startTime == nil) then
		--If not unset the stop time
		self.stopTime = nil;
	end
end

--This function resets all the variables. It is used if you want to use the same timer for multiple tests, but it shouldn't be necessary. Use to avoid unforseen bugs
function Stopwatch:reset()
	self.startTime = nil;
	self.stopTime = nil;
end

--Returns the difference in the time at the default time resolution
--Mostly called from the other get___ functions
function Stopwatch:getTime()
	local timeDifference = nil;
	--If start and stop has been called return the difference
	if((self.startTime ~= nil) and (self.stopTime ~= nil)) then
		timeDifference = self.stopTime - self.startTime;
	--If start has been called but stop hasn't make the current time the end time. This simulates looking at a stopwatch while it is still running
	elseif((self.startTime ~= nil) and (self.stopTime == nil)) then
		timeDifference = os.clock() - self.startTime;
	--Otherwise return -1 as an error
	else
		timeDifference = -1;
	end
	--Return the number generated
	return timeDifference;
end

--This function returns the duration the stopwatch has run in microseconds
function Stopwatch:getMicroseconds()
	local timeDifference = self:getTime();	--Get the time that has elapsed
	--If you did not get an error convert the time to microseconds
	if(timeDifference >= 0) then
		timeDifference = timeDifference * 1000000;
	end
	return timeDifference;
end

--This function returns the duration the stopwatch has run in milliseconds
function Stopwatch:getMilliseconds()
	local timeDifference = self:getTime();	--Get the time that has elapsed
	--If you did not get an error convert the time to milliseconds
	if(timeDifference >= 0) then
		timeDifference = timeDifference * 1000;
	end
	return timeDifference;
end

--This function returns the duration the stopwatch has run in seconds
function Stopwatch:getSeconds()
	--The function returns in seconds by default
	return self:getTime();
end

--This function returns the duration the stopwatch has run in minutes
function Stopwatch:getMinutes()
	local timeDifference = self:getTime();	--Get the time that has elapsed
	--If you did not get an error convert the time to minutes
	if(timeDifference >= 0) then
		timeDifference = timeDifference / 60;
	end
	return timeDifference;
end

--This function returns the duration the stopwatch has run in minutes
function Stopwatch:getMinutes()
	local timeDifference = self:getTime();
	--If you did not get an error conver the time to minutes
	if(timeDifference >= 0) then
		timeDifference = timeDifference / 60;
	end
	return timeDifference;
end

--This function returns the duration the stopwatch has run in hours
function Stopwatch:getHours()
	local timeDifference = self:getTime();	--Get the time that has elapsed
	--If you did not get an error convert the time to minutes
	if(timeDifference >= 0) then
		timeDifference = timeDifference / 3600;
	end
	return timeDifference;
end

function Stopwatch:getString()
	local timeDifference = self:getTime();	--Get the time that has elapsed
	local timeResolution = Stopwatch.timeResolution.seconds;
	local timeString = "";

	--Convert the time to a string with the format XXX.XXX <timeResolution>
	--The time is returned in seconds. Decide if that as ok or if the resolution needs to go up or down
	--If there are more than 120 seconds change the time to minutes
	if(timeDifference > 120) then
		timeDifference = timeDifference / 60;
		timeResolution = Stopwatch.timeResolution.minutes;
		--If there are more than 120 minutes change the time to hours
		if(timeDifference > 120) then
			timeDifference = timeDifference / 60;
			timeResolution = Stopwatch.timeResolution.hours;
		end

	--If there is less than 1 second change the time to milliseconds
	elseif(timeDifference < 1) then
		timeDifference = timeDifference * 1000;
		timeResolution = Stopwatch.timeResolution.milliSeconds;
		--If there are is less than 1 millisecond change the time to microseconds
		if(timeDifference < 1) then
			timeDifference = timeDifference * 1000;
			timeResolution = Stopwatch.timeResolution.microSeconds;
		end
	end
	--If the number of seconds is between 1 and 120 just use seconds

	--Create the string
	if(timeResolution == Stopwatch.timeResolution.microSeconds) then
		timeString = string.format("%.0f microseconds", timeDifference);
	elseif(timeResolution == Stopwatch.timeResolution.milliSeconds) then
		timeString = string.format("%.3f milliseconds", timeDifference);
	elseif(timeResolution == Stopwatch.timeResolution.seconds) then
		timeString = string.format("%.3f seconds", timeDifference);
	elseif(timeResolution == Stopwatch.timeResolution.minutes) then
		timeString = string.format("%.3f minutes", timeDifference);
	elseif(timeResolution == Stopwatch.timeResolution.hours) then
		timeString = string.format("%.3f hours", timeDifference);
	else
		timeString = "ERROR! TimeResolution = " .. timeResolution;
	end

	--Return the string
	return timeString;
end