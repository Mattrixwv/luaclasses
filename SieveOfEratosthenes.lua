--luaClasses/SieveOfEratosthenes.lua
--Matthew Ellison
-- Created: 06-30-21
--Modified: 06-30-21
--This creates a class that uses the Sieve of Eratosthenes to generate an infinite number of primes
--[[
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]


--Create the table
SieveOfEratosthenes = {};
SieveOfEratosthenes.__index = SieveOfEratosthenes;

--This is needed to create a Sieve and link the variables and functions
--It should be called before anything when creating a Sieve
function SieveOfEratosthenes:create()
	local sieve = {
		possiblePrime = 2;
		compositeMap = {};
	};
	setmetatable(sieve, SieveOfEratosthenes);	--This links the new variable with the functions from the SieveOfEratosthenes
	return sieve;
end

--This function returns the next prime number
function SieveOfEratosthenes:next()
	local prime = 0;
	if(self.possiblePrime > 2) then
		--Loop until you find a prime number
		while(self.compositeMap[self.possiblePrime] ~= nil) do
			--Create the next entry for all entries in the map
			for cnt = 1, #self.compositeMap[self.possiblePrime] do
				local num = self.compositeMap[self.possiblePrime][cnt];
				if(self.compositeMap[self.possiblePrime + num + num] == nil) then
					local tempArray = {num};
					self.compositeMap[self.possiblePrime + num + num] = tempArray;
				else
					table.insert(self.compositeMap[self.possiblePrime + num + num], num);
				end
			end
			--Delete the current entry
			self.compositeMap[self.possiblePrime] = nil;
			--table.remove(self.compositeMap, self.possiblePrime);
			self.possiblePrime = self.possiblePrime + 2;
		end
		--Save that the number is prime
		prime = self.possiblePrime;
		--Add the next entry to the prime
		if(self.compositeMap[prime * 3] == nil) then
			local tempArray = {prime};
			self.compositeMap[prime * 3] = tempArray;
		else
			table.insert(self.compositeMap[prime * 3], prime);
		end
		--Move on to the next possible prime
		self.possiblePrime = self.possiblePrime + 2;
	else
		--Return 2 and move to 3
		prime = self.possiblePrime;
		self.possiblePrime = self.possiblePrime + 1;
	end
	return prime;
end
