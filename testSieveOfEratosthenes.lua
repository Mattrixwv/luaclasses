--luaClasses/testSieveOfEratosthenes.lua
--Matthew Ellison
-- Created: 06-30-21
--Modified: 06-30-21
--This tests the SieveOfEratosthenes class
--[[
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

require "SieveOfEratosthenes"
require "Algorithms"


local function testSieveOfEratosthenes()
	local failed = false;	--Signals whether a test was failed

	--Test 1
	local sieve = SieveOfEratosthenes:create();
	local correctAnswer = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
	local answer = {};
	for _ = 0, 25 do
		table.insert(answer, sieve:next());
	end
	if(table.unpack(correctAnswer) ~= table.unpack(answer)) then
		io.write("SieveOfEratosthenes failed the test\n");
		failed = true;
	end

	--Print a message if all of the tests passed
	if(not failed) then
		io.write("SieveOfEratosthenes passed all tests\n");
	end
end

--Test SieveOfEratosthenes
testSieveOfEratosthenes();